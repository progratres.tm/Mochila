package mochila;

import java.util.ArrayList;
import java.util.Collections;

public class SolverGoloso 
{
	// Un solver est� siempre asociado con una instancia
	private Instancia _instancia;

	public SolverGoloso(Instancia instancia)
	{
		_instancia = instancia;
	}


	public Subconjunto resolver(Comparador comparador)
	{
		ArrayList<Objeto> ordenados = ordenar(comparador);
		return generarSolucion(ordenados);
	}

	private ArrayList<Objeto> ordenar(Comparador comparador)
	{
		// Ponemos los objetos en un ArrayList
		ArrayList<Objeto> ret = new ArrayList<Objeto>();
		for(int i=0; i<_instancia.cantidadDeObjetos(); ++i)
			ret.add( _instancia.objeto(i) );

		// Los ordenamos y los retornamos ordenados
		Collections.sort(ret, comparador);
		return ret;
	}

	private Subconjunto generarSolucion(ArrayList<Objeto> ordenados)
	{
		Subconjunto subconjunto = new Subconjunto(_instancia);
		for(Objeto obj: ordenados)
			if( subconjunto.entra(obj) )
				subconjunto.agregar(obj);

		return subconjunto;
	}
}
