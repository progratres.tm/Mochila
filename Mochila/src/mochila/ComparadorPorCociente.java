package mochila;

public class ComparadorPorCociente implements Comparador
{
	@Override
	public int compare(Objeto uno, Objeto otro) 
	{
		int cocienteUno = uno.beneficio() / uno.peso();
		int cocienteOtro = otro.beneficio() / otro.peso();
		return cocienteOtro - cocienteUno;
	}	
}
