package mochila;

public class Objeto 
{
	private String nombre;
	private int peso;
	private int beneficio;
	
	public Objeto(String n, int p, int b)
	{
		nombre = n;
		peso = p;
		beneficio = b;
	}
	
	public String nombre()
	{
		return nombre;
	}
	
	public int peso()
	{
		return peso;
	}

	public int beneficio()
	{
		return beneficio;
	}
}
