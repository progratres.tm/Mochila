package interfaz;

import genetico.GeneradorAleatorio;

import java.util.Random;

import genetico.Individuo;
import mochila.Instancia;
import mochila.Objeto;
import genetico.Poblacion;

public class MainClass
{
	public static void main(String[] args)
	{
		Instancia instancia = aleatoria(200);
		Individuo.setearGenerador(new GeneradorAleatorio());
		Poblacion poblacion = new Poblacion(instancia);
		Observador observadorConsola = new ObservadorPorConsola(poblacion);
		Observador observadorVisual = new ObservadorVisual (poblacion);
		poblacion.registrarObservador(observadorConsola);
		poblacion.registrarObservador(observadorVisual);

		poblacion.simular();
	}

	private static Instancia aleatoria(int n)
	{
		Random random = new Random(0);

		Instancia instancia = new Instancia(1000);
		//instancia.seteaCapacidad(100);
		
		for(int i=0; i<n; ++i)
		{
			int peso = random.nextInt(10) + 1;
			int benef = random.nextInt(10) + 1;

			instancia.agregarObjeto(new Objeto("Obj" + i, peso, benef));
		}
		return instancia;
	}
}
