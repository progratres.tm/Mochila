package interfaz;

import genetico.Individuo;
import genetico.Poblacion;


public class ObservadorPorConsola implements Observador
{
	private Poblacion _poblacion;
	
	public ObservadorPorConsola(Poblacion poblacion)
	{
		_poblacion = poblacion;
	}
	
	@Override
	public void notificar()
	{
		Individuo mejor = _poblacion.mejorIndividuo();
		System.out.print(" Fitnes del Mejor: " + mejor.fitness());
			
		System.out.print(" - Fitness Prom: " + _poblacion.fitnessPromedio());
			
		Individuo peor = _poblacion.peorIndividuo();
		System.out.println( " - Fitness del Peor: " + peor.fitness());
	}
}
