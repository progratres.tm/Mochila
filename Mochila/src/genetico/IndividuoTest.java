package genetico;

import static org.junit.Assert.*;
import mochila.Instancia;
import mochila.Objeto;

import org.junit.Test;

public class IndividuoTest 
{
	@Test
	public void mutarTest() 
	{
		
		Instancia instancia = setearInstancia();
		
		boolean[] bits = new boolean[] {true, true, false, true, true, false};
		
		Individuo.setearGenerador(new GeneradorTest(bits, 2));
		Individuo individuo = Individuo.aleatorio(instancia);
		individuo.mutar();
		
		boolean[] esperado  = new boolean[] {true, true, true, true, true, false};
		assertIguales(esperado, individuo);
	}

	@Test
	public void mutarPrimerBitTest() 
	{
		
		Instancia instancia = setearInstancia();
		
		boolean[] bits = new boolean[] {true, true, false, true, true, false};
		
		Individuo.setearGenerador(new GeneradorTest(bits, 0));
		Individuo individuo = Individuo.aleatorio(instancia);
		individuo.mutar();
		
		boolean[] esperado = new boolean[] {false, true, false, true, true, false};
		assertIguales(esperado, individuo);
	}

	@Test
	public void mutarUltimoBitTest() 
	{

		Instancia instancia = setearInstancia();
		
		boolean[] bits = new boolean[] {true, true, false, true, true, false};
		
		Individuo.setearGenerador(new GeneradorTest(bits, 5));
		Individuo individuo = Individuo.aleatorio(instancia);
		individuo.mutar();
		
		boolean[] esperado = new boolean[] {true, true, false, true, true, true};
		assertIguales(esperado, individuo);
	}
	
	@Test
	public void recombinarTest() 
	{
		Instancia instancia = setearInstancia();
		boolean[] bits = new boolean[] {true, true, false, true, true, false,
										false, true, true, false,false, true};
		Individuo.setearGenerador(new GeneradorTest(bits, 3));
		Individuo padre = Individuo.aleatorio(instancia);
		Individuo madre = Individuo.aleatorio(instancia);
		
		Individuo[] hijos = padre.recombinar(madre);
		
		boolean[] hesperado1 = new boolean[] {true, true, false, false, false, true};
		assertIguales(hesperado1, hijos[0]);
		
		boolean[] hesperado2 = new boolean[] {false, true, true, true, true, false};
		assertIguales(hesperado2, hijos[1]);

	}
	
	
	
	
	private Instancia setearInstancia() {
		Instancia instancia = new Instancia(10);
		instancia.agregarObjeto( new Objeto("x1",10,5));
		instancia.agregarObjeto( new Objeto("x2",5,8));
		instancia.agregarObjeto( new Objeto("x3",20,3));
		instancia.agregarObjeto( new Objeto("x4",4,22));
		instancia.agregarObjeto( new Objeto("x5",8,10));
		instancia.agregarObjeto( new Objeto("x6",14,9));
		return instancia;
	}

	private void assertIguales(boolean[] esperado, Individuo mutar) 
	{
		for (int i= 0 ; i < esperado.length; i++)
			assertEquals(esperado[i],mutar.getBit(i));
	}


}
