package genetico;

public interface Generador 
{
	public boolean nextBoolean();
	public int nextInt(int number);

}
