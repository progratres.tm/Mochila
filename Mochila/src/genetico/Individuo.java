package genetico;

import java.util.Arrays;
import mochila.Instancia;
import mochila.Objeto;

public class Individuo 
{
	private Instancia _instancia;
	private boolean[] _bits;
	private static Generador random;
	
	public static void setearGenerador(Generador generador)
	{
		random = generador;
	}
	
	private Individuo(Instancia instancia) 
	{
		_instancia = instancia;
		
		_bits = new boolean[instancia.cantidadDeObjetos()];
		
	}
	
	public static Individuo aleatorio (Instancia instancia)
	{
		Individuo individuo = new Individuo(instancia);

		for (int i=0; i< instancia.cantidadDeObjetos(); ++i)
			individuo.set(i,random.nextBoolean());
		
		return individuo;
	}
	
	public static Individuo vacio (Instancia instancia)
	{
		Individuo individuo = new Individuo(instancia);
		
		return individuo;
	}
	
	public void mutar()
	{
		int posicion = random.nextInt(_bits.length);
		_bits[posicion] = !_bits[posicion];
	}
	
	public Individuo[] recombinar(Individuo otro)
	{
		Individuo hijo1 = Individuo.vacio(_instancia);
		Individuo hijo2 = Individuo.vacio(_instancia);
		
		int corte = random.nextInt(_bits.length);
		
		for (int i=0; i<corte; ++i)
		{
			hijo1.set(i, this.getBit(i));
			hijo2.set(i, otro.getBit(i));
		}

		for (int i=corte; i<_bits.length; ++i)
		{
			hijo1.set(i, otro.getBit(i));
			hijo2.set(i, this.getBit(i));
		}
			
		return  new Individuo[]{hijo1, hijo2};
	}
	
	@Override
	public String toString() 
	{
		return "Individuo [_bits=" + Arrays.toString(_bits) + "]";
	}
	
	public static void main(String[] args) 
	{	
		Instancia instancia = InstanciaCampamento();
		Individuo.setearGenerador(new GeneradorAleatorio());
		Individuo ind = new Individuo(instancia);
		System.out.println(ind);
		ind.mutar();
		System.out.println(ind);
		System.out.println(ind.fitness());
	}

	private static Instancia InstanciaCampamento() 
	{
		double peso = 15;

		Instancia instancia = new Instancia(peso);
		
		instancia.agregarObjeto(new Objeto("Linterna",2,6));
		instancia.agregarObjeto(new Objeto("Encendedor",1,10));
		instancia.agregarObjeto(new Objeto("Comida",5,10));
		instancia.agregarObjeto(new Objeto("Bebida",3,8));
		instancia.agregarObjeto(new Objeto("Ropa",3,4));
		instancia.agregarObjeto(new Objeto("Repelente",1,2));
		instancia.agregarObjeto(new Objeto("Bolsa de Dormir",5,5));
		instancia.agregarObjeto(new Objeto("Cubiertos",1,2));
		instancia.agregarObjeto(new Objeto("Plato",2,2));
		instancia.agregarObjeto(new Objeto("Taza",1,1));
		instancia.agregarObjeto(new Objeto("Cantimplora",1,7));
		instancia.agregarObjeto(new Objeto("Equipo de Mate",4,6));
		return instancia;
	}
	
	
	// El fitness es negativo si no cumple la capacidad de la mochila
	public double fitness()
	{
		if( this.peso() <= _instancia.capacidad() )
			return this.beneficio();
		else
			return - this.peso() + _instancia.capacidad();		
	}
	
	public double beneficio()
	{
		double ret = 0;
		for (int i=0; i <_instancia.cantidadDeObjetos(); ++i)
			if (getBit(i) ==true)
				ret = _instancia.objeto(i).beneficio();

		return ret;
	}
	
	// Peso de un individuo
	public double peso()
	{
		double ret = 0;
		for (int i=0; i <_instancia.cantidadDeObjetos(); ++i)
			if (getBit(i) ==true)
				ret = _instancia.objeto(i).peso();
		return ret;
	}
	
	public boolean getBit(int i) 
	{
		return _bits[i];
	}
	
	private boolean set(int i, boolean bit) 
	{
		return _bits[i] = bit;
	}

}
