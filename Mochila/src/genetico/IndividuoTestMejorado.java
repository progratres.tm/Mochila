package genetico;

import static org.junit.Assert.*;
import mochila.Instancia;
import mochila.Objeto;
import org.junit.Before;
import org.junit.Test;

public class IndividuoTestMejorado 
{
	private Instancia instancia;
	
	@Before
	public void setearInstancia1() 
	{
		instancia = new Instancia(10);
		instancia.agregarObjeto( new Objeto("x1",10,5));
		instancia.agregarObjeto( new Objeto("x2",5,8));
		instancia.agregarObjeto( new Objeto("x3",20,3));
		instancia.agregarObjeto( new Objeto("x4",4,22));
		instancia.agregarObjeto( new Objeto("x5",8,10));
		instancia.agregarObjeto( new Objeto("x6",14,9));
	}
	
	@Test
	public void mutarTest() 
	{
		Individuo individuo = crearIndividuo("001100");
		mutar(individuo, 3);
		assertIguales("001000", individuo);
	}

	@Test
	public void mutarPrimerBitTest() 
	{		
		Individuo individuo = crearIndividuo("001100");
		mutar(individuo, 0);
		assertIguales("101100", individuo);
	}

	@Test
	public void mutarUltimoBitTest() 
	{		
		Individuo individuo = crearIndividuo("001100");
		mutar(individuo, 5);
		assertIguales("001101", individuo);
	}
	
	@Test
	public void recombinarTest() 
	{
		Individuo padre1 = crearIndividuo("001110");
		Individuo padre2 = crearIndividuo("110011");
		
		Individuo[] hijos = recombinar(padre1, padre2, 3);
		
		assertIguales("001011", hijos[0]);
		assertIguales("110110", hijos[1]);
	}
	
	@Test
	public void recombinarBit0Test() 
	{
		Individuo padre1 = crearIndividuo("001110");
		Individuo padre2 = crearIndividuo("110011");
		
		Individuo[] hijos = recombinar(padre1, padre2, 0);
		
		assertIguales("110011", hijos[0]);
		assertIguales("001110", hijos[1]);
	}

	@Test
	public void recombinarBitUltimoTest() 
	{
		Individuo padre1 = crearIndividuo("001110");
		Individuo padre2 = crearIndividuo("110011");
		
		Individuo[] hijos = recombinar(padre1, padre2, 5);
		
		assertIguales("001111", hijos[0]);
		assertIguales("110010", hijos[1]);
	}
	private Individuo crearIndividuo(String bits)
	{
		Individuo.setearGenerador(new GeneradorTest(asArray(bits), 0));
		return Individuo.aleatorio(instancia);
	}

	private void mutar(Individuo individuo, int i)
	{
		Individuo.setearGenerador(new GeneradorTest(null, i));
		individuo.mutar();
	}
	
	private void assertIguales(String esperados, Individuo individuo)
	{
		boolean[] bits = asArray(esperados);
		for(int i=0; i<bits.length; ++i)
			assertEquals(bits[i], individuo.getBit(i));
	}
		
	private Individuo[] recombinar(Individuo padre1, Individuo padre2, int i) 
	{
		Individuo.setearGenerador(new GeneradorTest(null, i));		
		return padre1.recombinar(padre2);

	}
	
	private boolean[] asArray(String bits)
	{
		boolean[] ret = new boolean[bits.length()];
		for(int i=0; i<ret.length; ++i)
			ret[i] = bits.charAt(i) == '1';
		
		return ret;
	}
}
